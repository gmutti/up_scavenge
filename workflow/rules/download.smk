rule get_pfam:
    output: 'results/meta/annotation/pfam.tsv.gz'
    shell: '''
wget "https://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.clans.tsv.gz" \
-O {output}
'''
rule get_proteomes:
    output: 'results/meta/proteomes.tsv'
    shell: '''
wget "https://rest.uniprot.org/proteomes/stream?fields=upid%2Corganism%2Corganism_id%2Cgenome_assembly\
%2Ccpd%2Cbusco%2Cprotein_count&format=tsv&query=%28*%29+AND+%28proteome_type%3A1%29" \
-O {output}
'''

rule download_unieuk:
    output:
        "results/meta/taxonomies/unieuk_taxonomy.tsv"
    shell:'''
wget https://eukmap.unieuk.net/exports/unieuk/1.0.0-first_release/unieuk-1.0.0-first_release.tsv -O {output}
'''

# Get EukProt metadata
rule get_eukprot:
    output:
        euk_included="results/meta/taxonomies/EukProt_included.tsv",
        euk_excluded="results/meta/taxonomies/EukProt_not_included.tsv"
    shell:'''
wget -O - https://figshare.com/ndownloader/files/34436249 | sed 's/\\"//g' > {output.euk_excluded}
wget -O - https://figshare.com/ndownloader/files/34436246 | sed 's/\\"//g' > {output.euk_included}
'''

rule get_temp_taxonomy:
    input: rules.get_proteomes.output
    output: 
        euk_lng='results/meta/taxonomies/lineage_euka_ncbi.tsv',
        noneuk='results/meta/taxonomies/taxonomy_noneuk.tsv'
    shell: '''
awk 'NR>1' {input} | cut -f1,3 | taxonkit lineage -i 2 | grep "Eukaryota;" > {output.euk_lng}

awk 'NR>1' {input} | cut -f1,3 | \
taxonkit reformat -I 2 | \
csvtk -H -t sep -f 3 -s ';' -R | grep -v "Eukaryota" | \
csvtk add-header -t -n proteome,taxid,kingdom,phylum,class,order,family,genus,species | \
csvtk sort -t -k 3,4,5,6,7,8 > {output.noneuk}
'''

rule get_ep_taxonomy:
    input:
        lineage=rules.get_temp_taxonomy.output.euk_lng,
        unieuk=rules.download_unieuk.output,
        ep=rules.get_eukprot.output.euk_included,
        notep=rules.get_eukprot.output.euk_excluded
    output: 'results/meta/taxonomies/taxonomy_ep.tsv'
    script: '../scripts/get_tax.R'

rule get_final_taxonomy:
    input:  
        euka=rules.get_ep_taxonomy.output,
        noneuka=rules.get_temp_taxonomy.output.noneuk
    output: 'results/meta/taxonomies/taxonomy.tsv'
    shell: 'cat {input.noneuka} {input.euka} > {output}'


checkpoint representative_proteomes:
    input: 
        tax=rules.get_final_taxonomy.output,
        meta=rules.get_proteomes.output
    output: 'results/meta/representative.ids'
    script: '../scripts/get_representative.R'

