import random

random.seed(42)

test_idxs = list(set([random.randint(0, 225) for _ in range(10)]))
anno_db = ["ec.number", "gene.ontology.ids", "gene3d","cdd", "pfam", "interpro"]#"kegg"
# anno_db = ["ec.number"]

def get_single_data(wildcards):
    with checkpoints.representative_proteomes.get(**wildcards).output[0].open() as f:
        proteomes = f.read().splitlines()
        # proteomes = [proteomes[i] for i in test_idxs]
        return expand("results/meta/single/{i}.tsv.gz", i=proteomes)


rule get_single_meta:
    output: "results/meta/single/{i}.tsv.gz"
    shell: '''
wget "https://rest.uniprot.org/uniprotkb/stream?compressed=true&fields=accession%2Cprotein_name%2Cgene_names\
%2Corganism_name%2Cec%2Cgo_id%2Clength%2Cmass%2Cxref_kegg%2Cxref_gene3d%2Cxref_cdd%2Cxref_pfam%2C\
xref_interpro&format=tsv&query=%28%28\
proteome%3A{wildcards.i}%29%29" -O {output}
'''

rule plot_hms:
    input:
        files=get_single_data,
        tax=rules.get_final_taxonomy.output
    params: 
        max_cols=config["max_cols"],
        min_seqs=config["min_seqs"]
    output: 'results/plots/{anno}.pdf'
    script: '../scripts/plot_hm.R'