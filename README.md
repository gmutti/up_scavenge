# UniProt scavenger project

```
module load hdf5 python/3.12.1 sqlite3 gcc/13.2.0 R/4.3.2 proj/9.3.1-gcc

snakemake --ri -p -k -j 2
```

Use the ultrarich metadata in UniProt to infer stuff.

## Domains taxonomic distribution


* ec.number: hierarcical nature
* GO: CC MF BP
* kegg
* gene3d: CATH hierarchy
* cdd
* pfam: Clans but not all of them
* interpro


# TODOs

* not the best sampling as it favors parasites
* https://ftp.ebi.ac.uk/pub/databases/interpro/current_release/ParentChildTreeFile.txt
* with EC maybe you can do some hierarchical stuff
* Exclude viruses?
* Maybe avoid heatmap and do something else, focus on kingdom specific domains etc
